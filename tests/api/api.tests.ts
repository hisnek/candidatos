import * as chai from 'chai';
import axios from "axios";

const expect = chai.expect;

describe('api.tests.ts', () => {

    describe('getTenantById', () => {
        it('deve retornar o tenant da empresa pelo seu id', async () => {

            const response = await axios.get("http://localhost:3000/test/getTenantById/t76d67f2-76d7-5152-a0b8-859ac258c5ab")
            expect(response.data).to.not.be.null
            expect(response.data.name).to.be.eq("Legacy")
            expect(response.data.id).to.be.eq("t76d67f2-76d7-5152-a0b8-859ac258c5ab")

        })
    })
})
