import 'source-map-support/register';

import {errorJSONResponse, formatJSONResponse} from '@libs/apiGateway';
import {middyfy} from '@libs/lambda';
import DynamodbUtils from "@libs/dynamodb.utils";
import {APIGatewayProxyHandler, APIGatewayProxyResult} from "aws-lambda";

/**
 * API exemplo
 * @param event
 */

const getTenantById: APIGatewayProxyHandler = async (event): Promise<APIGatewayProxyResult> => {

    console.info("Inicia servico getTenantById com evento", event)

    try {
        console.debug(`Recupera id do request para consulta no banco de dados`)
        const {id} = event.pathParameters

        if (!id) {
            console.warn(`Id nao informado ou invalido`)
            return errorJSONResponse({
                error: 'Parametro necessario id'
            })
        }

        console.debug(`Consulta banco de dados para id = [${id}]`)
        const response = await DynamodbUtils.getDocumentClient().query({
            TableName: "ivi-tenant-table-test",
            KeyConditionExpression: "#pk = :id",
            ExpressionAttributeNames: {
                "#pk": "pk"
            },
            ExpressionAttributeValues: {
                ":id": id
            }
        }).promise();


        console.debug(`Encontrado [${response.Count}] na consulta para o id [${id}]`, {response: JSON.stringify(response)})
        const {Items = []} = response;

        console.debug(`Extrai ultima posicao para capturar item desejado`, {Items})
        const Item = Items.pop()


        console.debug(`Renomeia atributos para nomes legiveis `)
        delete Object.assign(Item, {id: Item.pk }).pk;
        delete Object.assign(Item, {name: Item.sk }).sk;

        console.info(`Tenant recuperado com sucesso para id [${id}]`)
        return formatJSONResponse(Item);
    } catch (e) {
        console.error(e.message)
        throw errorJSONResponse({
            error: "Erro inesperado",
            msg: e.message
        })
    }
}

export const main = middyfy(getTenantById);
