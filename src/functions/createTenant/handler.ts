import 'source-map-support/register';

import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/apiGateway';
import { formatJSONResponse } from '@libs/apiGateway';
import { middyfy } from '@libs/lambda';

import schema from './schema';

/**
 * Todo [createTenant] Crie uma api que valida as informações de entrada do request e insere as informações no banco de dados e retona os valores criados para o usuário
 *
 * Requisitos:
 * O client deve mandar obrigatoriamente as seguintes informações:
 *
 * 1. name: string -> Nome da empresa
 * 2. architectureType: enum [SHARED | SILO] -> tipo de arquitetura a ser utilizada
 * 3. integrationKey: enum [IDENTITY | SAML | INTEGRATION] -> tipo de integração que deve ser utilizada
 * 4. autoCreateUser: boolean -> Determina se permite auto criação de um usuário
 *
 * O que deve ser atribuido autimaticamente:
 *
 * 1. type: enum = COMPANY
 * 2. createdAt: datetime
 * 3. updatedAt: datetime
 * 4. status: ACTIVE
 *
 * @param event
 */
const createTenant: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {

  // implementar logica
  return formatJSONResponse({
    message: `Hello ${event.body.name}, welcome to the exciting Serverless world!`,
  });
}

export const main = middyfy(createTenant);
