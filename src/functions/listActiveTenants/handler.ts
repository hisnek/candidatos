import 'source-map-support/register';

import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/apiGateway';
import { formatJSONResponse } from '@libs/apiGateway';
import { middyfy } from '@libs/lambda';

import schema from './schema';

/**
 * Todo [listActiveTenants] Crie uma api que lista somente os tenants com status = ACTIVE do banco de dados para o usuário
 * @param event
 */
const listActiveTenants: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {

  // implementar logica

  return formatJSONResponse({
    message: `Hello ${event.body.name}, welcome to the exciting Serverless world!`,
  });
}

export const main = middyfy(listActiveTenants);
