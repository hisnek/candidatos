import 'source-map-support/register';

import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/apiGateway';
import { formatJSONResponse } from '@libs/apiGateway';
import { middyfy } from '@libs/lambda';

import schema from './schema';

/**
 * Todo [updateTenantStatus] Crie uma api que valida atualiza o status para um dos seguintes valores abaixo do tenant pelo seu id e retorna os dados atualizados para o usuário
 *
 * status:
 * 1. ACTIVE
 * 2. INACTIVE
 * 3. REMOVED
 * 4. CANCELED
 *
 * @param event
 */
const updateTenantStatus: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {

  // implementar logica
  return formatJSONResponse({
    message: `Hello ${event.body.name}, welcome to the exciting Serverless world!`,
  });
}

export const main = middyfy(updateTenantStatus);
