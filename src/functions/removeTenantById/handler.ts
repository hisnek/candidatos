import 'source-map-support/register';

import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/apiGateway';
import { formatJSONResponse } from '@libs/apiGateway';
import { middyfy } from '@libs/lambda';

import schema from './schema';

/**
 * Todo [removeTenantById] Crie uma api que remove um tenant do banco de dados pelo seu id e retorna um objeto de sucesso ou erro para o usuário
 * @param event
 */
const removeTenantById: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {

  // implementar logica

  return formatJSONResponse({
    message: `Hello ${event.body.name}, welcome to the exciting Serverless world!`,
  });
}

export const main = middyfy(removeTenantById);
