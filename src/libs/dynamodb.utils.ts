import * as AWS from "aws-sdk";
import {DocumentClient} from "aws-sdk/clients/dynamodb";

export default class DynamodbUtils {
    private static readonly _localRegion = "localhost";
    private static readonly _localEndpoint = "http://localhost:8008";

    /**
     * Verifica se processo é offline
     * IS_OFFLINE é definido no arquivo de ambiente .env.local
     */
    static isOffline = (): string | undefined => {
        return process.env.IS_OFFLINE;
    };

    /**
     * Cria um DynamoDB Client
     */
    static getDocumentClient = (): DocumentClient => {
        return DynamodbUtils.isOffline()
            ? new AWS.DynamoDB.DocumentClient({
                region: DynamodbUtils._localRegion,
                endpoint: DynamodbUtils._localEndpoint,
            })
            : new AWS.DynamoDB.DocumentClient();
    };
}
