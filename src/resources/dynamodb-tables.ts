const tables = {
    TenantTable: {
        Type: 'AWS::DynamoDB::Table',
        DeletionPolicy: 'Retain',
        Condition: 'isTest',
        Properties: {
            TableName: '${self:custom.tables.tenant_table}',
            StreamSpecification: {StreamViewType: 'NEW_IMAGE'},
            AttributeDefinitions: [
                {AttributeName: 'pk', AttributeType: 'S'},
                {AttributeName: 'sk', AttributeType: 'S'}
            ],
            KeySchema: [
                {AttributeName: 'pk', KeyType: 'HASH'},
                {AttributeName: 'sk', KeyType: 'RANGE'},
            ],
            BillingMode: '${self:custom.tables.billing_mode}',
        }
    }
};

export default tables
