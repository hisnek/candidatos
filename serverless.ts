import type {AWS} from '@serverless/typescript';
import dynamodbTables from '@resources/dynamodb-tables';
import {createTenant, getTenantById, listActiveTenants, removeTenantById, updateTenantStatus} from "@functions/index";

const serverlessConfiguration: AWS = {
    service: 'candidato',
    frameworkVersion: '2',
    custom: {
        stage: '${opt:stage, self:provider.stage}',
        tables: {
            tenant_table: 'ivi-tenant-table-${opt:stage, self:provider.stage}',
            billing_mode: 'PAY_PER_REQUEST',
        },
        webpack: {
            packager: 'yarn',
            webpackConfig: './webpack.config.js',
            includeModules: {
                forceExclude: [
                    "aws-sdk"
                ]
            },
        },
        dynamodb: {
            stages: ['test'],
            start: {
                port: 8008,
                inMemory: true,
                migrate: true,
                seed: true,
                convertEmptyValues: true,
            },
            seed: {
                test: {
                    sources: [
                        {
                            table: '${self:custom.tables.tenant_table}',
                            sources: ['tests/seeds/tenant/tenant.test.data.json']
                        }
                    ]

                }
            }
        },
        ['serverless-offline']: {
            httpPort: 3000,
            lambdaPort: 3001,
            babelOptions: {
                presets: ["env"]
            }
        },
    },
    plugins: [
        'serverless-webpack',
        'serverless-dynamodb-local',
        'serverless-offline'
    ],
    provider: {
        name: 'aws',
        runtime: 'nodejs14.x',
        stage: 'dev',
        region: 'us-east-1',
        apiGateway: {
            minimumCompressionSize: 1024,
            shouldStartNameWithService: true,
        },
        environment: {
            AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
        },
        lambdaHashingVersion: '20201221',
    },
    resources: {
        Resources: {...dynamodbTables},
        Conditions: {
            isTest: {
                "Fn::Equals": ["${self:provider.stage}", "test"]
            }
        },
    },
    functions: {
        createTenant,
        getTenantById,
        listActiveTenants,
        removeTenantById,
        updateTenantStatus
    },
};

module.exports = serverlessConfiguration;
